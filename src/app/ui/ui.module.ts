import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { HeaderComponent } from './components/header/header.component';
import { UiComponent } from './containers/ui/ui.component';
import { MatIconModule, MatListModule,MatButtonModule , MatToolbarModule  } from '@angular/material';
import {MatSidenavModule} from '@angular/material/sidenav';


@NgModule({
  declarations: [NavComponent, HeaderComponent, UiComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule
  ],
  exports: [UiComponent]
})
export class UiModule { }
