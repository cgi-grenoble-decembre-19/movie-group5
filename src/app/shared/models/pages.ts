import { Film } from './film'

export class Pages {
  page: number;
  total_results: number;
  total_pages: number;
  results: Film[];

  constructor(fields?: Partial<Pages>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
