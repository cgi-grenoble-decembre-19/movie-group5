export class Film {
  popularity: number;
  vote_count: number;
  video: false;
  poster_path: string;
  id: number;
  adult: false;
  title: string;
  vote_average: number;
  overview: string;
  release_date: string;


  constructor(fields?: Partial<Film>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
