import { ListFilmsComponent } from './containers/list-films/list-films.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatDialog, MatDialogModule } from '@angular/material';


import { FilmRoutingModule } from './film-routing.module';
import { MatTableModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FilmDetailsComponent } from './components/film-details/film-details.component';

@NgModule({
  declarations: [ ListFilmsComponent, FilmDetailsComponent],
  imports: [
    CommonModule,
    FilmRoutingModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule
  ],
  entryComponents: [FilmDetailsComponent]
})
export class FilmModule { }
