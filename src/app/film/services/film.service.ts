import { Pages } from './../../shared/models/pages';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Film } from 'src/app/shared/models/film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) { }

  list() {
    return this.http.get<Pages>(`${environment.urlApi}movie/popular?${environment.keyApi}`).pipe(
      map((data) => {
        let page = new Pages(data);
        let films = [];
        data.results.map((film) => films.push(new Film(film)));
        page.results = films;

        return page;
      }
      ));
  }

  search(query: string) {
    return this.http.get<Pages>(`${environment.urlApi}search/movie?${environment.keyApi}&query=${query}`).pipe(
      map((data) => {
        let page = new Pages(data);
        let films = [];
        data.results.map((film) => films.push(new Film(film)));
        page.results = films;

        return page;
      }
      ));
  }


}
