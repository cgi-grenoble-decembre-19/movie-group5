import { FilmDetailsComponent } from './../../components/film-details/film-details.component';
import { Pages } from './../../../shared/models/pages';
import { FilmService } from './../../services/film.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Film } from 'src/app/shared/models/film';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged,switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.scss']
})
export class ListFilmsComponent implements OnInit {
  result: MatTableDataSource<Film>;
  page: Pages;
  displayedColumns: string[] = ['title', 'note'];
  public saisie$: Subject<string> = new Subject();

  constructor(private filmService: FilmService, private dialog: MatDialog) { }

  ngOnInit() {
    this.filmService.list().subscribe(
      (data) => {
        this.page = new Pages(data);
        console.log(this.page);
        this.result = new MatTableDataSource(this.page.results);
      }
    );
    this.setupSaisie();
  }
  setupSaisie() {
    this.saisie$.pipe(
      debounceTime(250),
      distinctUntilChanged(),
      switchMap((result) => this.filmService.search(result))

    ).subscribe(
      (data) => {
        this.page = new Pages(data);
        console.log(this.page);
        this.result = new MatTableDataSource(this.page.results);
      });
  }

  applyFilter(filterValue: string) {
    this.saisie$.next(filterValue);

  }
  search(term: string) {
    this.filmService.search(term).subscribe(
      (data) => {
        this.page = new Pages(data);
        console.log(this.page)
        this.result = new MatTableDataSource(this.page.results);
      }
    );
  }

  openDialog(film: Film): void {
    const dialogRef = this.dialog.open(FilmDetailsComponent, {
      width: '500px',
      data: film
    });
  }
}

