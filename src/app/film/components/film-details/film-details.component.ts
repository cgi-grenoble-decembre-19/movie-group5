import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Film } from 'src/app/shared/models/film';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit {
  imageUrl: string;


  constructor(private dialogRef: MatDialogRef<FilmDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data: Film) { }

  ngOnInit() {
    this.imageUrl = `http://image.tmdb.org/t/p/w300/${this.data.poster_path}`;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
